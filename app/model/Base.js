Ext.define('pancakeApp.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'pancakeApp.model'
    }
});
