Ext.define('pancakeApp.model.Customer', {
    extend: 'pancakeApp.model.Base',
    fields: [
        'pancakes', 'quantity'
    ]

});
