Ext.define('pancakeApp.model.Customer', {
    extend: 'pancakeApp.model.Base',

    fields: [
        'firstname', 'lastname', 'pancakes'
    ]

});
