Ext.define('pancakeApp.view.Login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLoginClick: function () {
        localStorage.setItem("LoggedIn", true);
        this.getView().destroy();
        Ext.widget('app-main');

    }
});