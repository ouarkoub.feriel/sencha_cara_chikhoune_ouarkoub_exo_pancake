Ext.define('pancakeApp.store.Cart', {
    extend: 'Ext.data.Store',

    alias: 'store.cart',



    data: {
        items: [
            { pancakes: 'banane', quantity: '30' },
            { pancakes: 'myrtille', quantity: '30' }

        ]
    },

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});