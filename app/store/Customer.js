Ext.define('pancakeApp.store.Customer', {
    extend: 'Ext.data.Store',

    alias: 'store.customer',


    data: {
        items: [
            { firstname: 'Sellenia', lastname: 'Chikhoune', pancakes: '3' },
            { firstname: 'Feriel', lastname: 'Ouarkoub', pancakes: '3' }

        ]
    },

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});