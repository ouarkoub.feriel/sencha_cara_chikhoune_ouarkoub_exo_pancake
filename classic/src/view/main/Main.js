Ext.define('pancakeApp.view.main.Main', {
    extend: 'Ext.container.Container',

    requires: [
        'pancakeApp.view.main.MainController',
        'pancakeApp.view.main.MainModel'
    ],

    xtype: 'app-main',

    controller: 'main',
    plugins: 'viewport',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
        xtype: 'panel',
        bind: {
            title: '{name}'
        },
        region: 'west',
        bind: {
            html: '{recipe}'
        },
        width: 250,
        split: true,
        tbar: [{
            text: 'Disconnect',
            handler: 'onClickButton'
        }]
    }, {
        region: 'center',
        xtype: 'tabpanel',
        items: [{
            title: 'Customers',
            xtype: 'mainlist',
            iconCls: 'fa fa-user'
        },
        {
            title: 'Cart',
            xtype: 'listpancakes',
            iconCls: 'fa fa-cart-plus'
        }
        ]
    }]
});