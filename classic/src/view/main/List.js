
Ext.define('pancakeApp.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'pancakeApp.store.Customer'
    ],

    title: 'Customer',

    store: {
        type: 'customer'
    },

    columns: [
        { text: 'FirstName', dataIndex: 'firstname', flex: 1 },
        { text: 'LastName', dataIndex: 'lastname', flex: 1 },
        { text: 'Pancakes', dataIndex: 'pancakes', flex: 1 }
    ],


});