Ext.define('pancakeApp.view.main.Cart', {
    extend: 'Ext.grid.Panel',
    xtype: 'listpancakes',

    requires: [
        'pancakeApp.store.Cart'
    ],

    title: 'Cart',

    store: {
        type: 'cart'
    },

    columns: [

        { text: 'Pancakes', dataIndex: 'pancakes', flex: 1 },
        { text: 'Quantity', dataIndex: 'quantity', flex: 1 }
    ],


});